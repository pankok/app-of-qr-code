import 'package:flutter/material.dart';

class InputText extends StatelessWidget {
  final String textLabel;
  final String textError;
  final TextInputType typeInput;
  final bool textObscure;

  const InputText({
    Key key,
    this.textLabel,
    this.textError,
    this.typeInput,
    this.textObscure = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 22),
      child: TextFormField(
        decoration: new InputDecoration(
          labelText: textLabel,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30),
          ),
        ),
        validator: (val) {
          if (val.length < 6) {
            if (!textObscure) {
              if (val.contains('@')) {
                return null;
              } else {
                return textError;
              }
            } else {
              return null;
            }
          } else {
            return null;
          }
        },
        obscureText: textObscure,
        keyboardType: typeInput,
      ),
    );
  }
}
