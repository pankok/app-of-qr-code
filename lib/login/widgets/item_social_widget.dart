import 'package:flutter/material.dart';

Widget itemSocial(String imageText, double widthValue) {
  return Container(
    margin: EdgeInsets.only(right: 20),
    child: Image.asset(
      imageText,
      width: widthValue,
    ),
  );
}
