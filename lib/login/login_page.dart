import 'package:flutter/material.dart';
import '../shared/button.dart';
import '../shared/styles.dart';
import 'widgets/input_login.dart';
import 'widgets/item_social_widget.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool alterPage = false;
  final _formKey = GlobalKey<FormState>();

  loginSearch() {
    if (_formKey.currentState.validate()) {
      setState(() {
        alterPage = true;
      });
      Future.delayed(Duration(seconds: 1), () {
        Navigator.pushReplacementNamed(context, '/home');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          AnimatedContainer(
            height: alterPage
                ? MediaQuery.of(context).size.height
                : MediaQuery.of(context).size.height * 0.4,
            decoration: BoxDecoration(
              color: Colors.blueAccent[400],
              borderRadius: BorderRadius.vertical(
                bottom:
                    alterPage ? Radius.circular(0.0) : Radius.circular(40.0),
              ),
            ),
            duration: Duration(seconds: 1),
            curve: Curves.fastOutSlowIn,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/logo.png",
                  height: 100,
                ),
                Text(
                  "Qr Store",
                  style: h5White,
                ),
              ],
            ),
          ),
          alterPage
              ? Container()
              : Container(
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InputText(
                            textLabel: "Email",
                            textError: "Email inválido",
                            typeInput: TextInputType.emailAddress),
                        InputText(
                            textLabel: "Senha",
                            textError: "Senha inválida",
                            typeInput: TextInputType.visiblePassword,
                            textObscure: true),
                        buttontext("Entrar", loginSearch),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Text("Entre também por:"),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            itemSocial("assets/images/face.png", 70.0),
                            itemSocial("assets/images/google.png", 50.0),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
