import 'package:flutter/material.dart';
import 'styles.dart';

Widget buttontext(String textButton, onpressd) {
  return Container(
    width: 280,
    height: 50,
    margin: EdgeInsets.all(40),
    child: RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30),
      ),
      color: Colors.blueAccent[400],
      onPressed: onpressd,
      child: Text(
        textButton,
        style: h6White,
      ),
    ),
  );
}
