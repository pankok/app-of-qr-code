import 'package:app_of_qrcode/home/widgets/input_valueqr.dart';
import 'package:app_of_qrcode/shared/button.dart';
import 'package:app_of_qrcode/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool alterPage = false;
  bool endAnimation = false;
  var maskFormatter = new MoneyMaskedTextController();
  String valueQr;
  final _formKeyHome = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        alterPage = true;
      });
      Future.delayed(Duration(seconds: 1), () {
        setState(() {
          endAnimation = true;
        });
      });
    });
  }

  generatorQr() {
    if (_formKeyHome.currentState.validate()) {
      setState(() {
        valueQr = maskFormatter.numberValue.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent[400],
        elevation: 0,
        title: endAnimation
            ? Text(
                "Qr Store",
                style: h6White,
              )
            : Text(""),
      ),
      body: endAnimation
          ? Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.blueAccent[400],
              child: Container(
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(40.0),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 40),
                      child: valueQr == null
                          ? Image.asset(
                              "assets/images/logo.png",
                              height: 90,
                            )
                          : QrImage(
                              embeddedImage:
                                  AssetImage("assets/images/logo.png"),
                              embeddedImageStyle: QrEmbeddedImageStyle(
                                size: Size(40, 40),
                              ),
                              foregroundColor: Colors.blueAccent[400],
                              data: valueQr,
                              version: QrVersions.min,
                              size: 200.0,
                            ),
                    ),
                    Form(
                      key: _formKeyHome,
                      child: Container(
                        margin: EdgeInsets.all(40),
                        child: InputValueQr(
                          textLabel: "Valor do pedido",
                          textError: 'Valor invalido',
                          typeInput: TextInputType.number,
                          textEditingController: maskFormatter,
                          alingText: TextAlign.right,
                        ),
                      ),
                    ),
                    Container(
                      child: buttontext("Gerar código QR", generatorQr),
                    )
                  ],
                ),
              ),
            )
          : AnimatedContainer(
              height: alterPage ? 0 : MediaQuery.of(context).size.height,
              color: Colors.blueAccent[400],
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
            ),
    );
  }
}
