import 'package:flutter/material.dart';

class InputValueQr extends StatelessWidget {
  final bool error;
  final String textLabel;
  final String textError;
  final TextInputType typeInput;
  final TextEditingController textEditingController;
  final bool textObscure;
  final TextAlign alingText;

  const InputValueQr(
      {Key key,
      this.textLabel,
      this.textError,
      this.typeInput,
      this.textEditingController,
      this.textObscure = false,
      this.alingText = TextAlign.left,
      this.error})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 22),
      child: TextFormField(
        decoration: new InputDecoration(
          labelText: textLabel,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30),
          ),
        ),
        validator: (val) {
          print(val);
          double valueQr = double.parse(val.replaceAll(new RegExp(','), '.'));
          if (valueQr < 1.00) {
            return textError;
          } else {
            return null;
          }
        },
        textAlign: alingText,
        controller: textEditingController,
        obscureText: textObscure,
        keyboardType: typeInput,
      ),
    );
  }
}
